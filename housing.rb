# frozen_string_literal: true

require 'bundler/inline'
require 'ostruct'
require 'pathname'
require 'json'
require 'httparty'
require 'nokogiri'
require 'forwardable'
require 'logger'
require 'erb'
require_relative 'email'
require_relative 'sheets'

class House
  extend Forwardable

  attr_accessor :data, :page_details, :today
  def_delegators :@data, :view_count, :coordinates, :description, :image_url, :beds, :baths, :link, :floor_area, :price, :address, :total_images

  def initialize(params)
    @default_details = {
      baths: '',
      beds: '',
      image_url: '',
      description: '',
      features: [],
      price: 0,
      coordinates: [0,0],
      floor_area: 999,
      total_images: 0
    }
    @data = OpenStruct.new(@default_details.merge(params))
  end

  def add_details(params = {})
    @data = OpenStruct.new(data.to_h.merge(params))
  end

  def last_update_date
    return Date.today if data.last_update_date.nil?

    last_update =  Date.parse(data.last_update_date)
    @today = Date.today == last_update

    last_update
  end


  def features
    data.features.map(&:downcase)
  end

  def to_hash
    {
      link: link,
      img_url: image_url,
      price: price,
      coordinates: coordinates,
      address: address,
      beds: beds,
      baths: baths,
      last_update_date: last_update_date,
      today: self.today
    }
  end

  def values
    [price, address, beds, baths, coordinates[1], coordinates[0], link]
  end
end

class EmailTemplate
  extend Forwardable
  attr_reader :erb, :houses

  def initialize(filename= 'listing.erb', houses)
    @erb = ERB.new(File.read(filename))
    @houses = houses
  end

  def email_content
    erb.result(binding)
  end
end


class Houses
  include Enumerable

  attr_reader :houses

  # @ array of House objects
  # TODO: optimize array size
  def initialize
    @houses = Array.new
  end

  def <<(details)
    tmp = House.new(details)

    return if  is_unwanted?(tmp.address) || tmp.link.empty? || tmp.floor_area < 40

    houses << tmp
  end

  def check_criteria
    houses.reject! do |x|
      x.total_images < 3 ||
      x.description.include?('ground floor') ||
      x.features.any? { |f| f.include?('ground floor') }
    end
  end

  def recent_houses
    houses.select { |h| h.today && h.view_count < 500 }
  end

  def each(&block)
    @houses.each(&block)
  end

  def unwanted_areas
    @untwanted_areas ||= ['newcastle', 'balbriggan', 'jobstown', 'lusk', 'cheryy orchard', 'ballyfermot']
  end


  def is_unwanted?(address)
    res = false
    unwanted_areas.each do |x|
      res = true if address.include?(x)
    end

    res
  end
end

class Scrubber
  SEARCH_STRING = ENV.fetch('SEARCH_TERM', 'https://www.daft.ie/property-for-sale/ireland?salePrice_to=250000&location=dublin&location=leixlip-kildare&pageSize=20')
  TOTAL_HOUSE = /(?<total_house>\d{1,})/
  SQUARE_METER = /(?<square_meter>\d{1,})/

  attr_accessor :logger, :mailService

  def initialize
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::INFO
    @mailService = EmailService.new
  end

  def total_images(details)
    return 0 if details['media'].nil? || details['media'].empty?

    details['media']['totalImages']
  end

  def img_url(details)
    return '' if details['media'].nil? || details['media'].empty?
    return '' if details['media']['images'].nil? || details['media']['images'].empty?
    return '' if details['media']['images'][0].nil?

    details['media']['images'][0].fetch('size360x240', '')
  end

  def views(stats)
    view_count = 0
    stats.each do |s|
      count = s.text
      if count.include?(',')
        view_count = count.gsub(',', '').to_i
      elsif count.include?('/')
        next
      else
        view_count = count.to_i
      end
    end

    view_count
  end

  def floor_area(data)
    return 0 if data.empty?

    data.match(Scrubber::SQUARE_METER)[:square_meter].to_i
  end

  def room(data)
    data.split(' ').first
  end

  def detail_page(data)
    return '' if data.nil? || data.empty?

    'https://www.daft.ie/' + data
  end

  def price(data)
    data.gsub('AMV', '')
  end

  def address(data)
    data.strip.downcase
  end

  def run
    response = HTTParty.get(SEARCH_STRING)
    html_doc = Nokogiri::HTML(response.body) if response.code == 200
    total_houseses_text = html_doc.css("h1[data-testid='search-h1']").text
    num_of_houses = total_houseses_text.match(TOTAL_HOUSE)[:total_house]
    logger.info "total number of houses #{num_of_houses}"
    extracted_houses = Houses.new

    offset = 0
     (num_of_houses.to_i / 20).times do |_|
      request_string = SEARCH_STRING + "&from=#{offset}"
      logger.info "Looking for #{request_string}"
      response = HTTParty.get(request_string)

      next unless response.code == 200

      html_doc = Nokogiri::HTML(response.body)
      houses = html_doc.css("ul[data-testid='results']").css('li')

      houses.each do |house|
       can =  {
         price: price(house.css("[data-testid='price']")&.text),
         address: address(house.css("[data-testid='address']")&.text),
         beds: room(house.css("[data-testid='beds']")&.text),
         floor_area: floor_area(house.css("[data-testid='floor-area']")&.text),
         baths: room(house.css("[data-testid='baths']")&.text),
         link: detail_page(house.css('a').attribute('href')&.value)
       }

      extracted_houses << can
      end
      offset = offset + 20
    end
    extracted_houses.each do |x|
      response = HTTParty.get(x.link)
      next unless response.code == 200

      html_doc = Nokogiri::HTML(response.body)
      stats = html_doc.css('.Statistics__StyledLabel-sc-15tgae4-1.pBjQg')
      house_data = JSON.parse(html_doc.css('#__NEXT_DATA__').text)
      details = house_data.dig('props', 'pageProps', 'listing')

      next if details.nil? || details.empty?

      page_details = {
        description: details.fetch('description', '').downcase,
        coordinates: details.dig('point', 'coordinates'),
        features: details.fetch('features', []),
        total_images: total_images(details),
        image_url: img_url(details),
        last_update_date: details.fetch('lastUpdateDate', Date.today),
        view_count: views(stats)
      }

      x.add_details(page_details)
    end

    extracted_houses.check_criteria

    # TODO improve performance
    convert =  extracted_houses.map(&:to_hash)

    if ENV['SHEET']
      csvs =  extracted_houses.map(&:values)
      SheetsService.new.save_housing_info(csvs)
    end

    recent_houses = extracted_houses.recent_houses

    if recent_houses.size == 0
      logger.info('No houses added to daft today')
    else
      mailService.send_mail(EmailTemplate.new('listing.erb', recent_houses).email_content)
    end

    write_results(convert)
  end

  # IO interface this
  def write_results(results)
    write_location = Pathname.new(Dir.pwd).join('_data')
    logger.info("#{write_location} does not exists") unless write_location.exist?

    File.open(write_location.join('index.json').to_s, 'w') do |f|
      f.write(JSON.dump(results))
    end
  end
end

sc = Scrubber.new
sc.run
