require 'googleauth'
require 'googleauth/stores/file_token_store'
require "google/apis/sheets_v4"
require "fileutils"
require 'tempfile'

class SheetsService
   OOB_URI = "urn:ietf:wg:oauth:2.0:oob".freeze
   APPLICATION_NAME = "Google Sheets API Ruby Quickstart".freeze
   CREDENTIALS_PATH = "credentials.json".freeze
   TOKEN_PATH = "sheets_token.yaml".freeze
   SCOPE = Google::Apis::SheetsV4::AUTH_SPREADSHEETS

  attr_accessor :service

  def initialize
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.client_options.application_name = APPLICATION_NAME
    @service.authorization = authorize
  end

  def save_housing_info(values = [])
    spreadsheet_id = "19OTXnVjm181rvRNuiYdeoXAX15yRyxqdkBCSPU5Fhkw"

    sheet_name = Date.today.to_s
    add_sheet_request = Google::Apis::SheetsV4::AddSheetRequest.new
    add_sheet_request.properties = Google::Apis::SheetsV4::SheetProperties.new
    add_sheet_request.properties.title = sheet_name
    batch_update_spreadsheet_request = Google::Apis::SheetsV4::BatchUpdateSpreadsheetRequest.new
    batch_update_spreadsheet_request.requests = [ add_sheet: add_sheet_request ]
    service.batch_update_spreadsheet(spreadsheet_id, batch_update_spreadsheet_request)
    range_name = "#{sheet_name}!A1:D1"
    columns = %w(price address bedrooms bathrooms	lat	long link)
    values = [columns] + values
    value_range_object = Google::Apis::SheetsV4::ValueRange.new(range:  range_name, values: values)
    result = service.append_spreadsheet_value(spreadsheet_id, range_name, value_range_object, value_input_option: 'RAW')
    puts "#{result.inspect} cells updated."
  end

  private
  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization. If authorization is required,
  # the user's default browser will be launched to approve the request.
  #
  # @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials
  def authorize
  # with environment variables
    client_id = nil
    if ENV['GOOGLE_SHEET_CLIENT_ID'] && ENV['GOOGLE_SHEET_CLIENT_SECRET']
      client_id = Google::Auth::ClientId.new(ENV['GOOGLE_SHEET_CLIENT_ID'], ENV['GOOGLE_SHEET_CLIENT_SECRET'])
    else
      client_id = Google::Auth::ClientId.from_file CREDENTIALS_PATH
    end

    token_store = Google::Auth::Stores::FileTokenStore.new file: token_file
    authorizer = Google::Auth::UserAuthorizer.new client_id, SCOPE, token_store
    user_id = "default"
    credentials = authorizer.get_credentials user_id
    if credentials.nil?
      url = authorizer.get_authorization_url base_url: OOB_URI
      puts "Open the following URL in the browser and enter the " \
           "resulting code after authorization:\n" + url
      code = gets
      credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: OOB_URI
      )
    end
    credentials
  end

  def token_file
    if ENV['sheets_token']
      file = Tempfile.new(TOKEN_PATH)
      content = <<-YAML
      ---
      #{ENV['sheets_token']}
      YAML

      file.write(content.strip)
      file.rewind

      file.path
    else
      TOKEN_PATH
    end
  end
end
